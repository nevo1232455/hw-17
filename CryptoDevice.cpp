#include "CryptoDevice.h"

byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

CryptoDevice::CryptoDevice(){};
CryptoDevice::~CryptoDevice(){};

string CryptoDevice::Aencrypt(string plainText)
{
	string eyText;

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);
	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(eyText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return (eyText);
}

string CryptoDevice::Adecrypt(string eyText)
{

	string deText;
	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);
	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(deText));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(eyText.c_str()), eyText.size());
	stfDecryptor.MessageEnd();

	return (deText);
}