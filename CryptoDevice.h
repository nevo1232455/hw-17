#pragma once
#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>
#include "des.h"

using namespace std;

class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	string Aencrypt(string);
	string Adecrypt(string);

};